<?php
session_start();

session_destroy();
setcookie("email","", time() - (10 * 365 * 24 * 60 * 60));
header('Location: login.php');