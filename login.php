<?php
include('common/database.php');

session_id();
session_start();

if(isset($_POST['submit'])){
	$flag=0;
	$email = $_POST['email']; 
	$password = $_POST['password'];

	$sql_query = "select * from users;"; 
	$result = mysqli_query($link,$sql_query);
	while($row = mysqli_fetch_assoc($result)){
		$db_email = $row['email'];
		$db_password = $row['password'];
		if($email == $db_email && $password == $db_password)
		{
			$flag=1;
			echo "<script>
        	window.location.href='index.php';
	        </script>";
	        
	         setcookie("email",$email,time() + (10 * 365 * 24 * 60 * 60));
	        $_SESSION['id'] = $row['id'];
		}
	}
	if($flag==0)
     {
 		echo "<script>
        alert('Incorrect Email or Password');
           window.location.href='login.php';
        </script>";
     } 
}

?>


    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Apply</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">

    </head>

    <body>
        <!--
<section id="top-section">
    <div class="contener">
        <div class="header-area">
            <nav class="navbar navbar-inverse">
                <div class="container">
                   
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div>

                    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="index.php">All <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">Complete</a></li>
                            <li><a href="#">Missed</a></li>
                            <li><a href="#">On Going</a></li> 
                            <li><a href="#">Rivisions</a></li>
                           <li><a href="#">Apply</a></li> 
                           <li><a href="#">Assign User</a></li> 
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</section> -->

        <!-- home Grid Section -->
        <section id="home" class="bg-light-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-2 text-center">

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="log-form">
                            <div class="logo text-center">
                                <a href="#"><img src="images/preneurlabb.png" width="200" height=""></a>

                            </div>
                            <div class="registration" style="margin-top: 40px;">

                                <form class="form-horizontal" action="login.php" method="post">
                                    <fieldset>

                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user" aria-hidden="true"></i></span>
                                            <input type="text" class="form-control" name="email" placeholder="Enter Your Email" style="text-align: left;" required>
                                        </div>
                                        <div class="clearfix"></div><br>
                                        <div class="input-group input-group-lg">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock" aria-hidden="true"></i></span>
                                            <input type="password" class="form-control" name="password" placeholder="Password" style="text-align: left" required>
                                        </div>
                                        <div class="clearfix"></div><br>

                                        <div class="col-md-12 text-center zero-padding">

                                            <button type="submit" name="submit" class="btn btn-default btn-lg" style="color: #EB1E25;">Sign In</button>

                                        </div>
                                    </fieldset>
                                </form>
                                <div class="foo text-center" style="color: #fff !important; text-decoration: none;">
                                    <a href="register.php">Sign Up</a>&nbsp;&nbsp;&nbsp; <span><a href="recover.php">Forgot Password</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 			<div class="footer text-center" style="color: #fff; margin-top: 180px; margin-bottom: 10px; border-top:1px solid #0000001; font-size: 10px;" >
			
				Powered by <a href="http://www.facebook.com/preneurlab">Preneur Lab</a>
			</div> -->
            </div>


        </section>

        <!-- start footer -->
        <?php include('common/footer.php'); ?>
