<?php
include('common/database.php');

include('common/session.php');


$vis= "SELECT team FROM users WHERE id=$id ";
	$recordvis = mysqli_query($link, $vis);
	$rowvis = mysqli_fetch_array($recordvis);
	$visi=$rowvis['team'];
// echo $visi;  
$sql = "SELECT  * FROM work_list WHERE visibility LIKE '%$visi%' AND status='complete' order BY last_date DESC";
$record = mysqli_query($link, $sql);
?>

 
<?php include('common/header.php'); ?>
<div class="container">
    <div class="logo text-center">
        <img src="images/preneurlabb.png">
    </div>

    <h2 class="text-uppercase text-center form-group">Applied Work List</h2>

    <div class="user-table">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Last Date</th>
                <th>Organization</th>
                <th>Description</th>
                <th>Link</th>
                <th>Assign To</th>
                <th>Visibility</th>
                <th>Status</th>
                <th>Action</th>  
            </tr>
            </thead>
            <tbody>
            <?php while ($emp = mysqli_fetch_assoc($record)) { ?>

                <tr>

                    <td><?php echo $emp['id'] ?></td>
                    <td>
                        <?php echo $emp['type'] ?>
                    </td>
                    <td><?php echo $emp['last_date'] ?></td>
                    <td><?php echo $emp['organization'] ?></td>
                    <td style="width:10%;"><?php echo $emp['description'] ?></td>
                    <td><?php echo '<a target="_blank" href="'.$emp['link'].'"><button>Link</button></a>' ?></td>
                    <td><?php echo $emp['assign_to']?></td>
                    <td><?php echo $emp['visibility']?></td>
                    <td><?php echo $emp['status']?></td>
                    <td>
                        <div class="btn-group">
                            <a href="status_update.php?id=<?php echo $emp['id'] ?>&btn=ongoing" class="btn btn-primary">On Going</a>
                            <a href="status_update.php?id=<?php echo $emp['id'] ?>&btn=complete" type="button"
                               class="btn btn-warning">Complete</a>
                            <a href="status_update.php?id=<?php echo $emp['id'] ?>&btn=missed" type="button" 
                               class="btn btn-danger">Missed</a>
                        </div>
                    </td>  
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <!--    <div class="pagination">
            <a href="#">&laquo;</a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">&raquo;</a>
        </div>  -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Details</h4>
            </div>
            <div class="user-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php include('common/footer.php'); ?>

