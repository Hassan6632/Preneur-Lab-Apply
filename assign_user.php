<?php include('common/session.php'); ?>


<?php include('common/header.php'); ?>
<!-- end header -->

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center text-uppercase">Add New Team User</h2>
            <div class="form-style-5">
                <form action="cust_regi.php" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Your Name *" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control" placeholder="Your Email*" required>
                        </div>
                        <div class="form-group apply-type-work">
                            <label for="job">Team:</label>
                            <select id="job" name="team">
									  <option value="">Please Select Your Team</option>
									  <option value="CEO">CEO</option>
									  <option value="Business Team">Business-Team</option>
									  <option value="Accounts Team">Accounts-Team</option>
									</select>
                            <!--			<br />
									<input style="margin-left: 55px;" type="checkbox" name="team[]" value="CEO" />CEO<br />
    									<input style="margin-left: 55px;" type="checkbox" name="team[]" value="Business-Team" />Business-Team<br />
    									<input style="margin-left: 55px;" type="checkbox" name="team[]" value="Accounts-Team" />Accounts-Team<br /> -->
                        </div>
                        <div class="form-group">
                            <input type="text" name="phone" class="form-control" placeholder="Your Phone number *" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Enter your password *" required>
                        </div>

                    </fieldset>
                    <div class="row">
                        <div class="col-md-4 col-md-offset-4 text-center">
                            <div class="form-group">
                                <input class="faul-tag-btn apply-work-submit-btn" type="submit" value="Submit" />
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- /.container -->

<!-- start footer -->
<?php include('common/footer.php'); ?>
<style>
    select#job {
        padding: 5px;
        border-radius: 5px;
    }

</style>
