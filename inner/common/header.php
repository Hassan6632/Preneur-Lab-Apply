<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Apply</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
 <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<section id="top-section">
    <div class="contener">
        <div class="header-area">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div>
                    

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                          <li class="active"><a href="index.php">All <span class="sr-only">(current)</span></a></li>
                            <li><a href="assign.php">Assign</a></li>
                             <li><a href="complete.php">Complete</a></li>
                            <li><a href="missed.php">Missed</a></li>
                            <li><a href="ongoing.php">On Going</a></li> 
                        <!--    <li><a href="#">Rivisions</a></li> -->
                           <li><a href="apply_work.php">Apply</a></li> 
                           <li><a href="assign_user.php">Assign User</a></li> 
                            <li><a href="logout.php">Logout</a></li>
                            
                        </ul>
                        
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
</section>
<!-- /#top-section -->