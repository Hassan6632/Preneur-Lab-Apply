<?php
include('common/database.php');
    session_start();
    //when user is logged in with session
    if (isset($_SESSION['id'])) {
       
      /*  SESSION SRARTS HERE  */
	include('plab/apply_assign.php');

      /*   SESSION ENDS HERE   */ 
       
    }else if(isset($_COOKIE['id'])){ //when user is logged with cookies
    
    
   	/*  COOKIES START HERE  */   

       include('plab/apply_assign.php');
        
       /*  COOKIES END HERE  */  
    }else{ //when user is not loggedin
        header("Location: login.php");
        exit();
    }
?>