
<?php include('common/header.php'); ?>
<!-- end header -->

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2 class="text-center text-uppercase">Add New Work</h2>
            	<div class="form-style-5">
							<form action="apply_work_done.php" method="post" enctype="multipart/form-data">
								<fieldset>
								<div class="form-group">
									<label for="job">Type of Work:</label>
									<select id="job" name="type">
									  <option value="Tender">Tender</option>
									  <option value="Grant">Grant</option>
									  <option value="Conference">Conference</option>
									</select>
								</div>	
								<div class="form-group">	
									<label for="job">Date:</label> <input type="text" name="date" class="form-control"  placeholder="Enter Date *"  id="datepicker"  required style="
    width: 30%;margin-left:40px;margin-top:-30px;">
								</div>	
								
								<div class="form-group">	
								<input type="text" name="organization" class="form-control"  placeholder="Enter Organization Name *" required>
								</div>
								
								<div class="form-group">	
								<input type="text" name="description" class="form-control"  placeholder="Enter Work Description *" required>
								</div>
								
								<div class="form-group">	
								<input type="text" name="work_link" class="form-control"  placeholder="Enter Work Link *" required>
								</div>
								
								<div class="form-group">	
									<label for="job">Assign To:</label>
							<!--		<select id="job" name="assign_to">
									 
									  
									   <?php
                          
                         							   $sql="SELECT * FROM users";
                         							   $result = mysqli_query($link, $sql);
                       								     while($row = mysqli_fetch_array($result))
                      								    {    
                      								      echo '<option value="'.$row['name'].'">'.$row['name'].'</option>';
                   	  					       	            }
                   							     ?>
									  
									  
									</select>  -->
									
									 <?php
                          
                         							   $sql="SELECT * FROM users";
                         							   $result = mysqli_query($link, $sql);
                       								     while($row = mysqli_fetch_array($result))
                      								    {    
                      								      echo '<input style="margin-left: 25px;margin-right: 5px;" type="checkbox" name="assign_to[]" value="'.$row['name'].'"/>'.$row['name'].'';
                   	  					       	            }
                   							     ?>
								</div>	
								
								<div class="form-group">	
									<label for="job">Visibility:</label>
									<!-- <select id="job" name="visibility">
									  <option value="All">All</option>
									  <option value="Chittagong">CEO</option>
									  <option value="Business Team">Business Team</option>
									  <option value="Accounts Team">Accounts Team</option>
									</select> -->
									
									<br />
									<input style="margin-left: 85px;" type="checkbox" name="visibility[]" value="CEO" />CEO<br />
    									<input style="margin-left: 85px;" type="checkbox" name="visibility[]" value="Business-Team" />Business-Team<br />
    									<input style="margin-left: 85px;" type="checkbox" name="visibility[]" value="Accounts-Team" />Accounts-Team<br />
								</div>	
								
									      
								</fieldset>
								<div class="form-group">
								<input type="submit" value="Submit" />
								</div>
							</form>
						</div>
        </div>
    </div>
</div>
<!-- /.container -->

<!-- start footer -->
<?php include('common/footer.php'); ?>
<style>
select#job{
	padding: 5px;
    	border-radius: 5px;
    	}
#ui-datepicker-div.ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all {

    position: absolute;
    top: 210px;
    left: 310px;
    z-index: 1;
    display: block;
    border: 2px solid #CCCCCC;
    background: #CCCCCC;
    border-radius: 10px;
    }   
    
    span.ui-icon.ui-icon-circle-triangle-w, span.ui-datepicker-month, span.ui-datepicker-year {
    
    padding:10px;
    }
    th {
    
    padding: 5px;
    }
    
</style>

 <script>
  $( function() {
    $( "#datepicker" ).datepicker();
  } );
  </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>