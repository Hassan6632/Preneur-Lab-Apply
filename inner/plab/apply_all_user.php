
<?php
$id=$_COOKIE['id'];
$sql = "SELECT  * FROM users order BY id DESC";
$record = mysqli_query($link, $sql);
?>


<?php include('common/header.php'); ?>
<div class="container">
    <h2 class="text-uppercase text-center form-group">Assigned User Details</h2>

    <div class="user-table">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Team</th>
                <th>Mobile</th>
             <!--   <th>Action</th> -->
            </tr>
            </thead>
            <tbody>
            <?php while ($emp = mysqli_fetch_assoc($record)) { ?>

                <tr>

                    <td><?php echo $emp['id'] ?></td>
                    <td><?php echo $emp['name'] ?></td>
                    <td><?php echo $emp['email'] ?></td>
                    <td><?php echo $emp['team'] ?></td>
                    <td><?php echo $emp['mobile'] ?></td>
                <!--    <td>
                        <div class="btn-group">
                            <a href="view.php?id=<?php echo $emp['id'] ?>" class="btn btn-primary">View</a>
                            <a href="u_edit.php?id=<?php echo $emp['id'] ?>" type="button"
                               class="btn btn-warning">Edit</a>
                            <a href="u_delete.php?id=<?php echo $emp['id'] ?>" type="button"
                               class="btn btn-danger">Delete</a>
                        </div>
                    </td> -->
                </tr>
            <?php } ?>
            </tbody>
        </table>
      <!--  <div class="pagination">
            <a href="#">&laquo;</a>
            <a href="#">1</a>
            <a class="active" href="#">2</a>
            <a href="#">3</a>
            <a href="#">4</a>
            <a href="#">5</a>
            <a href="#">6</a>
            <a href="#">&raquo;</a>
        </div> -->
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="viewuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">User Details</h4>
            </div>
            <div class="user-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php include('common/footer.php'); ?>

